# Recipe APP API Proxy

NGINX Proxy app for our recipe app API

## Usage

### Environment Variables 

* 'LISTEN_PORT' - POrt to listen on (Default: 8000)
* 'APP_HOST' - Hostname of the app to forward requests to (Default: 'app')
* 'APP_PORT' - Port of the app tp forward requests to (DEfault: 9000)
